<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
/**
 * @ORM\Entity(repositoryClass="App\Repository\UsuarioRepository")
 */
class Usuario
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $user_name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $user_pass;

    /**
     * @ORM\Column(type="datetime")
     */
    private $last_session;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Cliente", inversedBy="usuario_id", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $cliente_id;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Compra", mappedBy="usuario_id")
     */
    private $compra_id;

    public function __construct()
    {
        $this->compra_id = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUserName(): ?string
    {
        return $this->user_name;
    }

    public function setUserName(string $user_name): self
    {
        $this->user_name = $user_name;

        return $this;
    }

    public function getUserPass(): ?string
    {
        return $this->user_pass;
    }

    public function setUserPass(string $user_pass): self
    {
        $this->user_pass = $user_pass;

        return $this;
    }

    public function getLastSession(): ?\DateTimeInterface
    {
        return $this->last_session;
    }

    public function setLastSession(\DateTimeInterface $last_session): self
    {
        $this->last_session = $last_session;

        return $this;
    }

    public function getClienteId(): ?Cliente
    {
        return $this->cliente_id;
    }

    public function setClienteId(Cliente $cliente_id): self
    {
        $this->cliente_id = $cliente_id;

        return $this;
    }

    /**
     * @return Collection|Compra[]
     */
    public function getCompraId(): Collection
    {
        return $this->compra_id;
    }

    public function addCompraId(Compra $compraId): self
    {
        if (!$this->compra_id->contains($compraId)) {
            $this->compra_id[] = $compraId;
            $compraId->setUsuarioId($this);
        }

        return $this;
    }

    public function removeCompraId(Compra $compraId): self
    {
        if ($this->compra_id->contains($compraId)) {
            $this->compra_id->removeElement($compraId);
            // set the owning side to null (unless already changed)
            if ($compraId->getUsuarioId() === $this) {
                $compraId->setUsuarioId(null);
            }
        }

        return $this;
    }
}
