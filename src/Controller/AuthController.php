<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Usuario;

class AuthController extends AbstractController
{
    private $session;

    public function __construct(SessionInterface $session)
    {
        $this->session = $session;
    }
    
    /**
     * @Route("/iniciar-sesion", name="login")
     */
    public function login()
    {
        $carro = $this->session->all();
        unset($carro['user_sess']);
        return $this->render('auth/login.html.twig', [
            'count_car' => count($carro)
        ]);
    }
    
    /**
     * @Route("/auth", name="auth", methods="POST")
     */
    public function authUser(Request $request) 
    {
        $user_log = $request->request->get('usuario');
        $pass_log = $request->request->get('password');
        
        $repository = $this->getDoctrine()->getRepository(Usuario::class);
        $user = $repository->findOneBy([
            'user_name' => $user_log,
            'user_pass' => $pass_log
        ]);
        
        if ($user) {
            $this->session->set('user_sess', $user);
            return $this->redirectToRoute('carrito');
        }
        
        $this->addFlash('notice', true);
        return $this->redirectToRoute('login');
    }
    
    /**
     * @Route("/logout", name="logout")
     */
    public function logout() {
        $this->session->remove('user_sess');
        return $this->redirectToRoute('inicio');
    }
}
