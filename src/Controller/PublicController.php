<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Articulo;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class PublicController extends AbstractController
{
    private $session;
    private $count_carrito;

    public function __construct(SessionInterface $session)
    {
        $this->session = $session;
        $this->count_carrito = count($this->session->all());
    }
    
    /**
     * @Route("/", name="inicio")
     */
    public function index()
    {
        $articulos = $this->getDoctrine()->getRepository(Articulo::class);
        $carro = $this->session->all();
        unset($carro['user_sess']);

        return $this->render('public/index.html.twig', [
            'articulos' => $articulos->findAll(),
            'count_car' => count($carro)
        ]);
    }
    
    /**
     * @Route("/contacto", name="contacto")
     */
    public function contacto()
    {
        $carro = $this->session->all();
        unset($carro['user_sess']);
        return $this->render('public/contacto.html.twig', [
            'count_car' => count($carro)
        ]);
    }
    
    /**
     * @Route("/carro", name="carrito")
     */
    public function carro()
    {
        $carro = $this->session->all();
        unset($carro['user_sess']);

        return $this->render('public/carro.html.twig', [
            'productos' => $this->session->all(),
            'count_car' => count($carro)
        ]);
    }
    
    /**
     * @Route("/detalle/{id}", name="detalles")
     */
    public function detalle(Articulo $articulo)
    {
//        $articulo = $this->getDoctrine()->getRepository(Articulo::class)->find($id);
        if (!$articulo) {
            return $this->redirectToRoute('inicio');
        }
        
        return $this->render('public/detalle.html.twig', [
            'articulo' => $articulo,
            'count_car' => $this->count_carrito
        ]);
    }
    
    /**
     * @Route("/bien-hecho", name="complete")
     */
    public function completo() {
        return $this->render('carro/completado.html.twig', [
            'count_car' => $this->count_carrito
        ]);
    }
}
