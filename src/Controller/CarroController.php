<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpFoundation\Request;

use App\Entity\Articulo;
use App\Entity\Cliente;
use App\Entity\Compra;
use App\Entity\Usuario;

class CarroController extends AbstractController
{
    private $session;

    public function __construct(SessionInterface $session)
    {
        $this->session = $session;
    }
    
    /**
     * @Route("/addtocart/{idItem}", name="carro", methods="GET")
     */
    public function addToCart($idItem)
    {
        $articulo = $this->getDoctrine()->getRepository(Articulo::class)->find($idItem);
        if (!$articulo) {
            return $this->redirectToRoute('inicio');
        }
        
        $key = $this->uniqueId();
        $this->session->set("carrito{$key}", $articulo);
        return $this->redirectToRoute('inicio');
    }
    
    /**
     * @Route("/deleteitem/{id}", name="delete_item")
     */
    public function deleteItem($id) 
    {
        $this->session->remove($id);
        return $this->redirectToRoute('carrito');
    }
    
    /**
     * @Route("/datos_cliente", name="datosCliente")
     */
    public function datosCliente() 
    {
        $carro = $this->session->all();
        unset($carro['user_sess']);
        return $this->render('carro/datos_cliente.html.twig', [
            'count_car' => count($carro),
            'articulos' => $this->session->all()
        ]);
    }
    
    /**
     * @Route("/pagarcompra", name="pagarCompra", methods="POST")
     */
    public function pagarCompra(Request $request) {
        $clienteDoc = $this->getDoctrine()->getRepository(Cliente::class);
        $entityManager = $this->getDoctrine()->getManager();
        $clienteMan = $clienteDoc->findOneBy([
            'email' => $request->request->get('cliEmail')
        ]);
        if (!$clienteMan) {
            $cliente = new Cliente();
            $cliente->setNombre($request->request->get('cliNombre'));
            $cliente->setApaterno($request->request->get('cliApat'));
            $cliente->setDireccion($request->request->get('cliDir'));
            $cliente->setEstado($request->request->get('cliEstados'));
            $cliente->setCodigoPostal((int)$request->request->get('cliCp'));
            $cliente->setEmail($request->request->get('cliEmail'));
            $cliente->setTelefono($request->request->get('cliTel'));
            $entityManager->persist($cliente);
            $entityManager->flush();
            
            $usuario = new Usuario();
            $usuario->setUserName($request->request->get('cliNombre'));
            $usuario->setUserPass($request->request->get('cliEmail'));
            $usuario->setLastSession(new \DateTime());
            $usuario->setClienteId($cliente);
            $entityManager->persist($usuario);
            $entityManager->flush();
            
            $clienteObj = $usuario;
        }
        else {
            $clienteObj = $clienteMan->getUsuarioId();
        }
        
        $compra = new Compra();
        $compra->setTotal($request->request->get('total'));
        $compra->setFecha(new \DateTime());
//            $compra->setUsuarioId($cliente->getUsuarioId()->getId());
        $compra->setUsuarioId($clienteObj);

        $entityManager->persist($compra);
        $entityManager->flush();

        $sesiones = $this->session->all();
        foreach ($sesiones as $key => $sesion) {
            if ($key != 'user_sess') $this->session->remove($key);
        }
        return $this->redirectToRoute('complete');
    }
    
    private function uniqueId() 
    {
        $unique = time() . rand(1000, 9999);
        return md5($unique);
    }
}